package com.example.bhavin.dilogue;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class dilogue extends Dialog implements android.view.View.OnClickListener {
    Button b1,b2;
    Activity a;
    public dilogue(Activity activity) {
        super(activity);
        this.a=activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dilogue);

        b1=findViewById(R.id.b1);
        b2=findViewById(R.id.b2);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.b1:
                a.finish();
                break;
            case R.id.b2:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}
